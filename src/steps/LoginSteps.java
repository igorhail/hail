package steps;

import PageObjects.LoginPage;
import PageObjects.Widgets.MenuBarHorizontal;

public class LoginSteps {

    public void openLoginPage() {
        new MenuBarHorizontal().clickLoginButton();
    }

    public void login(String login, String password) {
        LoginPage loginPage = new LoginPage();
        loginPage.fillLogin(login);
        loginPage.fillPassword(password);
        loginPage.clickEnter();
    }

}
