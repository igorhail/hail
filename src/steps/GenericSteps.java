package steps;

import PageObjects.LoginPage;
import generic.WebDriverContext;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

public class GenericSteps {

    WebDriver driver = WebDriverContext.getDriver();

    public void openMainPage() {
        //driver.manage().window().maximize();
        driver.get("http://test.dirtmixer.com");
        driver.manage().window().setSize(new Dimension(1600, 900));
        String currentWindow = driver.getWindowHandle();
        driver.switchTo().window(currentWindow);
    }

    public void openLoginPageDirectly() {
        //driver.manage().window().maximize();
        driver.get("http://test.dirtmixer.com/signin");
        driver.manage().window().setSize(new Dimension(1600, 900));
        String currentWindow = driver.getWindowHandle();
        driver.switchTo().window(currentWindow);
    }
}
