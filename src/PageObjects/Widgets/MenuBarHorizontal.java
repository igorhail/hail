package PageObjects.Widgets;

import ElementWrappers.LocatorFinder;
import org.openqa.selenium.WebElement;

public class MenuBarHorizontal {

    String btnLogin = "//a[@href='/signin' and @class='button']";
    String btnAva = "avatar_header";

    private WebElement getBtnUserProfile(){
        return LocatorFinder.findElementByXpath(btnLogin);
    }
    private WebElement getAvaBtn(){
        return LocatorFinder.findElementByID(btnAva);
    }

    public void clickLoginButton(){
        getBtnUserProfile().click();
    }

    public void clickAvaButton(){
        getAvaBtn().click();
    }
}
