package PageObjects;

import generic.WebDriverContext;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {

    WebDriver driver;

    public Page(){
        driver =  WebDriverContext.getDriver();
        waitForPageToLoad(driver);
    }

    public static void waitForPageToLoad(final WebDriver driver) {
        ExpectedCondition<Boolean> pageLoad = driver1 ->
                "complete".equals(((JavascriptExecutor) driver1).executeScript("return document.readyState"));
        Wait<WebDriver> wait = new WebDriverWait(driver, 60);
        try {
            wait.until(pageLoad);
        } catch (WebDriverException pageLoadWaitError) {
            //LOGGER.log(ConfigurationLevel.SEVERE, "Timeout during page load", pageLoadWaitError);
        }
    }
}
