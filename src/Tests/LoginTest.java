import Objects.Racer;
import Objects.User;
import generic.WebDriverContext;
import org.testng.annotations.Test;
import steps.LoginSteps;
import steps.UserProfileSteps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.testng.Assert.assertEquals;

public class LoginTest extends WebDriverContext {

    @Test
    public void loginTest() {
        LoginSteps loginSteps = new LoginSteps();
        loginSteps.openLoginPage();
        User user = new Racer("Grad");
        loginSteps.login(user.getName(), user.getPassword());
        new UserProfileSteps().clickUserProfileAvatar();
        assertEquals(driver.getCurrentUrl(), "http://test.dirtmixer.com/profile");
    }

}
