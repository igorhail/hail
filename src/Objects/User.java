package Objects;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;

@Setter
@Getter
public abstract class User {

    String name;
    String password;

    Properties prop;

    final static Logger logger = Logger.getLogger(User.class);

    public User(String name){
        try (InputStream input = new FileInputStream("src/TestData/UserAccounts.properties")) {

            prop = new Properties();

            // load a properties file
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("LOG4J : " + ex);
        }
        setName(prop.getProperty(name + ".name"));
        setPassword(prop.getProperty(name + ".password"));
    }
}
